import { model, Schema } from 'mongoose';

export const GroupDatabase = model(
	'Group',
	new Schema(
		{
			name: Schema.Types.Mixed,
			type: String,
			avatar: Schema.Types.Mixed,
			activityAt: Number,
			updatedAt: Number,
		},
		{ minimize: false }
	)
);
