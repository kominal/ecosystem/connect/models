import { Message } from '../message/message';
import { Profile } from '@kominal/ecosystem-models/profile';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface Group {
	cryptoState?: 'VIEWABLE' | 'RESOLVED';

	id: string;
	joined: number;
	left: number;
	role: string;
	type: 'DM' | 'GROUP';
	partnerId?: string;
	activityAt: number;
	updatedAt: number;
	unreadMessages: number;

	name?: AESEncrypted;
	avatar?: AESEncrypted;
	groupKey: number[];
	partnerProfileId?: string;
	latestMessageId?: string;

	nameDecrypted?: string;
	keyDecrypted?: string;
	avatarDecrypted?: any;
	partnerProfile?: Profile;
	latestMessage?: Message;
}
