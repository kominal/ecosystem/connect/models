import { model, Schema } from 'mongoose';

export const MembershipDatabase = model(
	'Membership',
	new Schema(
		{
			groupId: Schema.Types.ObjectId,
			userId: Schema.Types.ObjectId,
			profileId: Schema.Types.ObjectId,
			profileKey: Schema.Types.Mixed,
			joined: Number,
			left: Number,
			removed: Number,
			role: String,
			groupKey: [Number],
			updatedAt: Number,
			unreadMessages: Number,
			latestMessageId: String,
		},
		{ minimize: false }
	)
);
