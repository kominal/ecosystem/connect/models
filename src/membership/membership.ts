import { Profile } from '@kominal/ecosystem-models/profile';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface Membership {
	cryptoState?: 'VIEWABLE';

	id: string;
	groupId: string;
	userId: string;
	profileId: string;
	profileKey: AESEncrypted;
	joined: number;
	left: number;
	role: string;
	updatedAt: number;
	profile?: Profile;
	connectionId?: string;
}
