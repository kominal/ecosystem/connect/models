import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface Attachment {
	storageId?: string;
	name?: AESEncrypted;
	type?: AESEncrypted;
	size?: AESEncrypted;
	imageStorageId?: string;
	imageSize?: AESEncrypted;

	nameDecrypted?: string;
	typeDecrypted?: string;
	sizeDecrypted?: string;
	image?: any;
	imageSizeDecrypted?: { height: number; width: number };
	arrayBuffer?: ArrayBuffer;
}
