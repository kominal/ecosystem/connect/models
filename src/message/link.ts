import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface Link {
	url?: AESEncrypted;
	site?: AESEncrypted;
	title?: AESEncrypted;
	description?: AESEncrypted;
	imageStorageId?: string;
	imageSize?: AESEncrypted;

	urlDecrypted?: string;
	siteDecrypted?: string;
	titleDecrypted?: string;
	descriptionDecrypted?: string;
	image?: any;
	imageSizeDecrypted?: { height: number; width: number };
}
