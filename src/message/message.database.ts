import { model, Schema } from 'mongoose';

export const LinkSchema = new Schema(
	{
		url: Schema.Types.Mixed,
		site: Schema.Types.Mixed,
		title: Schema.Types.Mixed,
		description: Schema.Types.Mixed,
		imageStorageId: Schema.Types.ObjectId,
		imageSize: Schema.Types.Mixed,
	},
	{ minimize: false }
);

export const AttachmentSchema = new Schema(
	{
		name: Schema.Types.Mixed,
		size: Schema.Types.Mixed,
		type: Schema.Types.Mixed,
		imageStorageId: Schema.Types.ObjectId,
		imageSize: Schema.Types.Mixed,
		storageId: Schema.Types.ObjectId,
	},
	{ minimize: false }
);

export const MessageDatabase = model(
	'Message',
	new Schema(
		{
			userId: Schema.Types.ObjectId,
			groupId: Schema.Types.ObjectId,
			time: Number,
			text: Schema.Types.Mixed,
			links: [LinkSchema],
			attachments: [AttachmentSchema],
		},
		{ minimize: false }
	)
);
