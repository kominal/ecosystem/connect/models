import { Link } from './link';
import { Attachment } from './attachment';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';
import { Membership } from '../membership/membership';

export interface Message {
	cryptoState?: 'TEXT' | 'ENCRYPTED' | 'VIEWABLE' | 'RESOLVED';

	id?: string;
	batchId?: string;
	groupId: string;
	userId: string;
	status: 'SENDING' | 'SENT' | 'FAILED';
	time: number;
	attachments: Attachment[];
	links: Link[];
	text?: AESEncrypted;

	textDecrypted?: string;
	textParsed?: string;
	timeString?: string;
	element?: any;
	membership?: Membership;
}
