import { model, Schema, Types } from 'mongoose';

const MetaDataDatabase = model(
	'MetaData',
	new Schema(
		{
			userId: Types.ObjectId,
			storeableId: Types.ObjectId,
			created: Date,
			iv: [Number],
		},
		{ minimize: false }
	)
);

export default MetaDataDatabase;
