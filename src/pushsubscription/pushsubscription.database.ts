import { model, Schema, Types } from 'mongoose';

export const PushSubscriptionDatabase = model(
	'PushSubscription',
	new Schema(
		{
			userId: Types.ObjectId,
			token: String,
			device: Schema.Types.Mixed,
			created: Number,
		},
		{ minimize: false }
	)
);
