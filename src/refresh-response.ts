import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface RefreshResponse {
	userId: string;
	jwt: string;
	jwtExpires: number;
	masterEncryptionKey: AESEncrypted;
	privateKey: AESEncrypted;
	publicKey: AESEncrypted;
}
