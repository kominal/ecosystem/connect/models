export enum Status {
	OFFLINE = 'Offline',
	AWAY_FROM_KEYBOARD = 'Away',
	DO_NOT_DISTURB = 'Do no disturb',
	ONLINE = 'Online',
}
