import { model, Schema, Types } from 'mongoose';

const StorageDatabase = model(
	'Data',
	new Schema(
		{
			userId: Types.ObjectId,
			created: Date,
			data: [Number],
			iv: [Number],
		},
		{ minimize: false }
	)
);

export default StorageDatabase;
