export interface StorageDecrypted {
	id: string;
	userId: string;
	created: Date;
	data: any;
}
