import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface StorageEncrypted {
	id: string;
	userId: string;
	created: Date;
	data: AESEncrypted;
}
